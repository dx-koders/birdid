# README

This is the bird-id project.

This project contains the bird id application implemented in several different languages. The application is used to teach programming and to show the differences between languages.


### Setup ###

Create a copy of the `env.template` file, naming it `.env` and update the information within accordingly.
