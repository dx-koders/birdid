# README


### Setup ###

- we recommend using ruby-install to manage ruby environments, see 'Install A Ruby Version' for more info
- install packages: `bundle`


### Install A Ruby Version ###

```
# install ruby-build
git clone https://github.com/rbenv/ruby-build.git
$ sudo PREFIX=/usr/local ./ruby-build/install.sh

# list ruby versions
ruby-build --definitions

# install a ruby version
mkdir -p ~/.envs/ruby && ruby-build 2.7.2 ~/.envs/ruby/2.7.2

# add environment to path (I generally use `~/.bash_profile` by adding something like the following:

    # ruby-build
    PATH="$PATH:$HOME/.envs/ruby/3.0.0/bin"
# after you add it make sure you `source ~/.bash_profile`

# verify test your ruby is available/working
ruby --version
```

### Links ###

- [ruby-build](https://github.com/rbenv/ruby-build)
- [dotenv](https://rubygems.org/gems/dotenv/versions/2.1.1)
    - [dotenv docs](https://www.rubydoc.info/gems/dotenv/2.1.1)
