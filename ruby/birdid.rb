#!/usr/bin/env ruby
# run: ruby birdid.rb

require 'artii'
require 'dotenv'
require 'mongo'
require 'tomlrb'

# Turn off debug-mode for mongo driver.
Mongo::Logger.logger.level = Logger::WARN

Dotenv.load(File.join(".", "../.env"))
$conf = Tomlrb.load_file('../data/config.toml', symbolize_keys: true)
client = Mongo::Client.new($conf[:data][:mongo][:url])
$db = client.database
$user = "#{ENV['BIRD_USER']}"
if not $user or $user.length < 3
    abort("User is not valid!\n - user must be specified and must be at least 3 characters long.")
end

def format_bird(bird, fmt)
    if fmt == "short"
        return "- #{bird['name'].capitalize}"
    elsif fmt == "compact"
        return "- #{bird['name'].capitalize}: #{bird['size']}, #{bird['color']}, #{bird['habitat']}, #{bird['eats_insects']}, #{bird['rank']}, #{bird['comment']}, #{bird['source']}, #{bird['created_by']}"
    elsif fmt == "full"
        return %Q(
#{bird['name'].capitalize}
    - size:      #{bird['size']}
    - color:     #{bird['color']}
    - habitat:   #{bird['habitat']}
    - eats bugs: #{bird['eats_insects']}
    - rank:      #{bird['rank']}
    - comment:   #{bird['comment']}
    - source:    #{bird['source']}
    - creator:   #{bird['created_by']})
    else
        print("Error in formatting bird, unknown format: '#{fmt}'")
        return ""
    end
end

def format_number(in_str)
    return in_str.to_s.reverse.scan(/.{1,3}/).join(',').reverse
end

def show_header(title)
    puts("\n#{$conf[:display][:header_text]} #{title} #{$conf[:display][:header_text]}")
end

def show_birds(title, flt, lmt=20)
    bird_sort = {'$natural' => -1}
    if $conf[:data][:sort][:birds] == 'alphabetical'
        bird_sort = {'name' => 1}
    end
    show_header(title)
    $db[:birds].find(flt, :sort => bird_sort).limit(lmt).each do |bird|
        puts format_bird(bird, $conf[:display][:bird_line_format])
    end
end

def first_bird()
    bird_sort = {}
    if $conf[:data][:sort][:birds] == 'alphabetical'
        bird_sort = {'name' => 1}
    end
    return $db[:birds].find({}, :sort => bird_sort).limit(1).first[:name]
end

def last_bird()
    bird_sort = {'_id' => -1}
    if $conf[:data][:sort][:birds] == 'alphabetical'
        bird_sort = {'name' => -1}
    end
    $db[:birds].find().sort(bird_sort).limit(1).first[:name]
end

def get_user_input(prompt)
    print(prompt)
    return gets.chomp
end

def get_bird(bird_name)
    return $db[:birds].find({"name": bird_name}).limit(1).first
end

def add_bird(response)
    if response.downcase == "y"
        bird_name = get_user_input("  What is the bird name? ").downcase
        bird_size = get_user_input("  What is the bird size? ").downcase
        bird_color = get_user_input("  What is the bird color? ").downcase
        bird_habitat = get_user_input("  What is the bird habitat? ").downcase
        bird_ei = get_user_input("  Does this bird eat insects? [y/n] ").downcase
        bird_rank = get_user_input("  What is the bird's rank (9 being best)? [0-9] ").to_i
        bird_comment = get_user_input("  Any comments about the bird? ")

        bird_eats_insects = bird_ei.start_with? 'y'
        confirm = get_user_input(%Q(Confirm Add Bird:
  - name: #{bird_name}
  - size: #{bird_size}
  - color: #{bird_color}
  - habitat: #{bird_habitat}
  - eat insects: #{bird_eats_insects}
  - rank: #{bird_rank}
  - comment: #{bird_comment}
  - source: birdid
Are you sure you want to add/update the bird? [y/n]: ))
        if confirm.downcase == "y"
            $db[:birds].update_one(
                {"name": bird_name},
                {'$set':
                    {"name": bird_name, "size": bird_size, "color": bird_color, "habitat": bird_habitat, "eats_insects": bird_eats_insects, "rank": bird_rank, "comment": bird_comment, "source": "birdid", "created_by": $user}
                },
                { :upsert => true }
            )
            puts("Bird '#{bird_name.capitalize}' has been added!")

        else
            puts("Add/update bird canceled.")
        end
        add_another_response = get_user_input("Do you want to add/update another bird? [y/n]: ")
        add_bird(add_another_response)
    end
end

def delete_bird(response)
    if response.downcase == "y"
        bird_name = get_user_input("  Name of bird to delete: ").downcase
        bird = get_bird(bird_name)
        if not bird
            puts("Cannot Delete Bird!\n  Bird not found.")
        elsif bird['created_by'] != $user
            puts("Cannot Delete Bird!\n  You did not add '{bird[:name]}', it was created by: {bird[:created_by]}")
        else
            puts("  - deleting bird: {bird[:name]}")
            result = $db[:birds].delete_one({"name": bird[:name]})
            puts("    - deleted #{result.deleted_count} bird")
        end
        delete_another = get_user_input("Do you want to delete another bird? [y/n]: ").downcase
        delete_bird(delete_another)
    end
end

def lookup_bird(response)
    if response.downcase == "y"
        bird_name = get_user_input("  Name of bird to find: ").downcase
        bird = get_bird(bird_name)
        if not bird
            puts("\nBird not found.")
        else
            puts(format_bird(bird, "full"))
        end
        lookup_another = get_user_input("Do you want to find another bird? [y/n]: ").downcase
        lookup_bird(lookup_another)
    end
end

puts($conf[:display][:logo])
puts("#{$conf[:about][:name]} - v#{$conf[:about][:ruby][:version]} (modified: #{$conf[:about][:ruby][:modified]}) - © #{$conf[:about][:ruby][:authors]}")
welcome_msg = Artii::Base.new :font => $conf[:display][:user_logo_font]
puts welcome_msg.asciify("Hi #{$user}!")
puts($conf[:display][:separator])

number_of_load_birds = 0
if $conf[:options][:show_load_birds]
    number_of_load_birds = get_user_input("Enter number of load birds: ").to_i
end
show_big_birds = "n"
if $conf[:options][:show_load_birds]
    show_big_birds = get_user_input("Do you want to show big birds? [y/n]: ")
end
show_insect_eating_birds = get_user_input("Do you want to show birds who eat insects (y) or birds who do not eat insects(n)? [y/n]: ")
add_bird_response = get_user_input("Do you want to add/update a bird? [y/n]: ")
add_bird(add_bird_response)
delete_bird_response = get_user_input("Do you want to delete a bird? [y/n]: ")
delete_bird(delete_bird_response)
lookup_bird_response = get_user_input("Do you want to look up a bird? [y/n]: ")
lookup_bird(lookup_bird_response)
puts($conf[:display][:separator])

puts("- Total Birds: #{format_number($db[:birds].count_documents({}))}")
puts("- First Bird:  #{first_bird()}")
puts("- Last Bird:   #{last_bird()}")

if $conf[:options][:show_all_birds]
    show_birds("All Birds", {}, 100)
end
if $conf[:options][:show_small_birds]
    show_birds("Small Birds", {"size": "small"}, 100)
end
if $conf[:options][:show_bright_birds]
    show_birds("Bright Birds", {"color": "bright"}, 100)
end

if show_big_birds == "y"
    show_birds("Big Birds", {"size": "large"}, 100)
end

if show_insect_eating_birds == "y"
    show_birds("Buggers", {"eats_insects": true}, 100)
else
    show_birds("No Buggers", {"eats_insects": false}, 100)
end

if number_of_load_birds > 0
    show_header("Load Birds: #{format_number(number_of_load_birds)}")
    start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    lb = []
    for i in (0...number_of_load_birds) do   # note the 0... (three dots - try just two and see what you get)
        lb.push({ "name": "Testee #{i}", "size": "medium", "color": "light", "habitat": "everywhere", "eats_insects": true, "rank": 5, "comment": "created via load_bird", "created_by": $user })
        if (i % $conf[:data][:insert_queue_amount]) == 0
            $db[:birds].insert_many(lb, {:ordered => false})
            lb = []
        end
    end

    if lb.length() < 0
        $db[:birds].insert_many(lb, {:ordered => false})
    end

    puts("- Bird Count: #{format_number($db[:birds].count_documents({}))}")
    puts("- Last Bird:  #{last_bird()}")
    finish = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    puts("- Load Time:  #{finish - start}")

    if $conf[:data][:cleanup_load_birds]
        puts("- cleaning up load_birds data...")
        dc = $db[:birds].delete_many({"name": { "$regex": "Testee*"}})
        puts("  - deleted: #{dc.deleted_count}")
    end
end

show_header("RUN SUMMARY")
puts("- Bird Count:         #{format_number($db[:birds].count_documents({}))}")
puts("- Load Birds:         #{format_number(number_of_load_birds)}")
puts("- Cleanup Load Birds: #{$conf[:data][:cleanup_load_birds]}")
puts("- iQA:                #{format_number($conf[:data][:insert_queue_amount])}")
