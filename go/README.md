# README

You will need to install go before you can compile the app.

### Install ###

- see the [install instructions](https://golang.org/doc/install)


### Run App ###

- run app: `go run birdid.go`
    - first run may take a little longer as it needs to install dependencies

### Setup DB ###

See the README.md in `mongo/` for setup info.

### Useful Links ###

- [go-toml github](https://github.com/pelletier/go-toml):
    note: [viper](github.com/spf13/viper): likely a better library to use but more than what we need currently
- [jackc/pgx](https://github.com/jackc/pgx): low-level pg driver in go
- [go-pg](https://github.com/go-pg/pg): ORM with pg focus
    + could not get this to work (nothing in db after usage!)
- [gorm](https://github.com/go-gorm/gorm): seems to be defacto orm package for go
    + works fine (note: go-pg claims to be 2x faster)
    + [gorm docs](https://gorm.io/docs/index.html)
