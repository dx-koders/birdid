module birdid

go 1.15

require (
	github.com/common-nighthawk/go-figure v0.0.0-20200609044655-c4b36f998cf2 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	go.mongodb.org/mongo-driver v1.4.5 // indirect
	gorm.io/driver/postgres v1.0.6 // indirect
	gorm.io/gorm v1.20.11 // indirect
)
