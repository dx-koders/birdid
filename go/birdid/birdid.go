package main

import (
    "github.com/pelletier/go-toml"
    "go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "github.com/joho/godotenv"
    "github.com/common-nighthawk/go-figure"
    "bufio"
    "context"
    "fmt"
    "log"
    "math"
    "math/rand"
    "os"
    "strconv"
    "strings"
    "time"
)

var birds *mongo.Collection
var ctx = context.TODO()
var reader = bufio.NewReader(os.Stdin)

type Bird struct {
    Name string                 `json:"name,omitempty"`
    Size string                 `json:"size,omitempty"`
    Color string                `json:"color,omitempty"`
    Habitat string              `json:"habitat,omitempty"`
    Eats_insects bool           `json:"eats_insects,omitempty"`
    Rank uint8                  `json:"rank,omitempty"`
    Comment string              `json:"comment,omitempty"`
    Source string               `json:"source,omitempty"`
    Created_by string           `json:"created_by,omitempty"`
}

func (bird Bird) String() string {
    // short
    return fmt.Sprintf("%s: %s", strings.Title(bird.Name), bird.Comment)
    // full
    //~ return fmt.Sprintf("- %s, size: %s, color: %s, habitat: %s, eats_insects: %v, rank: %d, comment: %s", bird.Name, bird.Size, bird.Color, bird.Habitat, bird.Eats_insects, bird.Rank, bird.Comment)
}

func format_header(title string, header_text string) string {
    return fmt.Sprintf("\n%s %s %s\n", header_text, title, header_text);
}

func init() {
    // note: you need to manually create the db and the mongo/mongo user first!
    clientOptions := options.Client().ApplyURI("mongodb://mongo:mongo@bob:27017/db")
    client, err := mongo.Connect(ctx, clientOptions)
    if err != nil {
        log.Fatal(err)
    }
    err = client.Ping(ctx, nil)
    if err != nil {
        log.Fatal(err)
    }
    birds = client.Database("db").Collection("birds")
}

func show_birds(label string, filter bson.D, header_text string, config *toml.Tree) {
    fmt.Print(format_header(label, header_text))
    opts := options.Find().SetSort(bson.D{{"$natural", -1}})
    if config.Get("data.sort.birds") == "alphabetical" {
        opts = options.Find().SetSort(bson.D{{"name", 1}})
    }
    cursor, err := birds.Find(ctx, filter, opts)
    if err != nil {
        log.Fatal(err)
    }
    defer cursor.Close(ctx)
    for cursor.Next(ctx) {
        var bird Bird
        if err = cursor.Decode(&bird); err != nil {
            log.Fatal(err)
        }
        fmt.Println(format_bird(bird, config.Get("display.bird_line_format").(string)))
    }
}

// @TODO: fix this so its not a hack.
func show_birds_m(label string, filter bson.M, header_text string, config *toml.Tree) {
    fmt.Print(format_header(label, header_text))
    opts := options.Find().SetSort(bson.D{{"$natural", -1}})
    if config.Get("data.sort.birds") == "alphabetical" {
        opts = options.Find().SetSort(bson.D{{"name", 1}})
    }
    cursor, err := birds.Find(ctx, filter, opts)
    if err != nil {
        log.Fatal(err)
    }
    defer cursor.Close(ctx)
    for cursor.Next(ctx) {
        var bird Bird
        if err = cursor.Decode(&bird); err != nil {
            log.Fatal(err)
        }
        fmt.Println(format_bird(bird, config.Get("display.bird_line_format").(string)))
    }
}

func last_bird(config *toml.Tree) Bird {
    opts := options.FindOne().SetSort(bson.D{{"$natural", -1}})
    if config.Get("data.sort.birds") == "alphabetical" {
        opts = options.FindOne().SetSort(bson.D{{"name", -1}})
    }
    var bird Bird
    err := birds.FindOne(ctx, bson.D{{}}, opts).Decode(&bird)
    if err != nil {
        log.Fatal(err)
    }
    return bird
}

func count_birds() int64 {
    count, err := birds.CountDocuments(ctx, bson.M{}, nil)
    if err != nil {
        log.Fatal("Error getting count:", err)
    }
    return count
}

func get_user_input(prompt string) string {
    fmt.Print(prompt)
    response, _ := reader.ReadString('\n')
    response = strings.TrimSuffix(response, "\n")
    return response
}

func get_bird(bird_name string, user string) Bird {
    var bird Bird
    birds.FindOne(ctx, bson.D{{"name", bird_name}}).Decode(&bird)
    return bird
}

func delete_bird(response string, user string) {
    if strings.ToLower(response) == "y" {
        bird_name := strings.ToLower(get_user_input("  Name of bird to delete: "))
        bird := get_bird(bird_name, user)
        if bird.Created_by != user {
            fmt.Printf("Cannot Delete Bird!\n  You did not add '%s', it was created by: %s\n", bird.Name, bird.Created_by)
        } else {
            fmt.Println("  - deleting bird:", bird.Name)
            result, err := birds.DeleteOne(ctx, bson.D{{"name", bird.Name}})
            if err != nil {
                log.Fatal(err)
            }
            fmt.Printf("    - deleted %v bird\n", result.DeletedCount)
        }
        delete_another := get_user_input("Do you want to delete another bird? [y/n]: ")
        delete_bird(delete_another, user)
    }
}

func add_bird(response string, user string) {
    if strings.ToLower(response) == "y" {
        bird_name := strings.ToLower(get_user_input("  What is the bird name? "))
        bird_size := strings.ToLower(get_user_input("  What is the bird size? "))
        bird_color := strings.ToLower(get_user_input("  What is the bird color? "))
        bird_habitat := strings.ToLower(get_user_input("  What is the bird habitat? "))
        bird_ei := strings.ToLower(get_user_input("  Does this bird eat insects? [y/n] "))
        bird_rank, _ := strconv.ParseUint(get_user_input("  What is the bird's rank (9 being best)? [0-9] "), 10, 32)
        bird_comment := get_user_input("  Any comments about the bird? ")

        bird_eats_insects, _ := strconv.ParseBool(bird_ei)
        confirm_msg := fmt.Sprintf(`
Confirm Add/Update Bird:
 - name: %s
 - size: %s
 - color: %s
 - habitat: %s
 - eats insects: %t
 - rank: %d
 - comment: %s
Are you sure you want to add the bird? [y/n] `, bird_name, bird_size, bird_color, bird_habitat, bird_eats_insects, bird_rank, bird_comment)
        confirm := get_user_input(confirm_msg)
        if strings.ToLower(confirm) == "y" {
            bird := Bird{
                Name: bird_name,
                Size: bird_size,
                Color: bird_color,
                Habitat: bird_habitat,
                Eats_insects: bird_eats_insects,
                Rank: uint8(bird_rank),
                Comment: bird_comment,
                Source: "birdid",
                Created_by: user,
            }
            opts := options.Update().SetUpsert(true)
            filter := bson.D{{"name", bird_name}}
            update := bson.D{{"$set", bird}}
            _, err := birds.UpdateOne(ctx, filter, update, opts)
                if err != nil {
                    log.Fatal(err)
                }
            fmt.Println(fmt.Sprintf("Bird '%s' has been added/updated!", bird_name))
            add_another := get_user_input("Do you want to add/update another bird? [y/n]: ")
            add_bird(add_another, user)
        } else {
            fmt.Println("Add/Update bird canceled.")
        }
    }
}

func format_bird(bird Bird, format string) string {
    if format == "short" {
        return fmt.Sprintf("- %s", strings.Title(bird.Name))
    } else if format == "name-and-color" {
        return fmt.Sprintf("- %s: %s", strings.Title(bird.Name), bird.Color)
    } else if format == "compact" {
        return fmt.Sprintf("- %s: %s, %s, %s, %t, %d, %s, %s, %s", strings.Title(bird.Name), bird.Size, bird.Color, bird.Habitat, bird.Eats_insects, bird.Rank, bird.Comment, bird.Source, bird.Created_by)
    } else if format == "full" {
        return fmt.Sprintf(`
%s
    - size: %s
    - color: %s
    - habitat: %s
    - eats insects: %t
    - rank: %d
    - comment: %s
    - source: %s
    - creator: %s`, strings.Title(bird.Name), bird.Size, bird.Color, bird.Habitat, bird.Eats_insects, bird.Rank, bird.Comment, bird.Source, bird.Created_by)
    } else {
        log.Fatal("Unknown format for format_bird: ", format)
    }
    return ""
}


func lookup_bird(response string, user string) {
    if strings.ToLower(response) == "y" {
        bird_name := strings.ToLower(get_user_input("  What is the bird name? "))
        bird := get_bird(bird_name, user)
        if (bird != Bird{}) {
            fmt.Println(format_bird(bird, "full"))
        } else {
            fmt.Printf("\nBird not found: '%s'\n", bird_name)
        }
        lookup_another := get_user_input("Do you want to lookup another bird? [y/n]: ")
        lookup_bird(lookup_another, user)
    }
}

func main() {
    _ = godotenv.Load("../../.env")
    config, err := toml.LoadFile("../../data/config.toml")
    if err != nil {
        panic("Error loading config!")
    }
    //~ fmt.Printf("config is of type: %T\n", config);
    user := strings.TrimSpace(os.Getenv("BIRD_USER"))
    if len(user) < 3 {
        panic("User is not valid!\n - user must be specified and must be at least 3 characters long.")
    }
    user_ascii := figure.NewFigure(fmt.Sprintf("Hi %s!", user), config.Get("display.user_logo_font").(string), true)

    insert_queue_amount := float64(config.Get("data.insert_queue_amount").(int64))
    header_text := config.Get("display.header_text").(string)

    fmt.Println(config.Get("display.logo").(string))
    fmt.Printf("   %s - v%s (%s) - © %s\n\n",
        config.Get("about.name"),
        config.Get("about.go.version"),
        config.Get("about.go.modified"),
        config.Get("about.go.authors"))
    fmt.Printf(config.Get("display.separator").(string))
    user_ascii.Print()
    fmt.Println("")

    var number_of_load_birds uint64 = 0
    if config.Get("options.show_load_birds").(bool) {
        number_of_load_birds, _ = strconv.ParseUint(get_user_input("Enter number of load birds: "), 10, 32)
    }
    var show_big_birds string = "n"
    if config.Get("options.show_big_birds").(bool) {
        show_big_birds = get_user_input("Do you want to show big birds? [y/n]: ")
    }
    show_insect_eaters := get_user_input("Do you want to show birds that eat insects (y) or birds that do not eat insects (n)? [y/n]: ")
    show_own_birds := get_user_input("Do you want to show all of your birds? ")
    add_bird_response := get_user_input("Do you want to add/update a bird? [y/n]: ")
    add_bird(add_bird_response, user)
    delete_bird_response := get_user_input("Do you want to delete a bird? [y/n]: ")
    delete_bird(delete_bird_response, user)
    lookup_bird_response := get_user_input("Do you want to look up a bird? [y/n]: ")
    lookup_bird(lookup_bird_response, user)

    fmt.Printf(config.Get("display.separator").(string));

    var first_bird Bird
    err = birds.FindOne(ctx, bson.D{{}}, options.FindOne().SetSort(map[string]int{"name": 1})).Decode(&first_bird)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Println("- Total Birds:", count_birds())
    fmt.Println("- First Bird: ", strings.Title(first_bird.Name))
    fmt.Println("- Last Bird:  ", strings.Title(last_bird(config).Name))

    if config.Get("options.show_all_birds").(bool) {
        show_birds("All Birds", bson.D{{}}, header_text, config)
    }

    if config.Get("options.show_small_birds").(bool) {
        show_birds("Small Birds", bson.D{{"size", "small"}}, header_text, config)
    }

    if config.Get("options.show_bright_birds").(bool) {
        show_birds("Bright Birds", bson.D{{"color", "bright"}}, header_text, config)
    }

    if show_big_birds == "y" {
        show_birds("Big Birds", bson.D{{"size", "large"}}, header_text, config)
    }

    if show_insect_eaters == "y" {
        show_birds_m("Buggers", bson.M{"eats_insects": bson.M{"$eq": true},}, header_text, config)
    } else {
        show_birds_m("No Buggers", bson.M{"eats_insects": bson.M{"$eq": false},}, header_text, config)
    }
    if show_own_birds == "y" {
        show_birds("Your Birds", bson.D{{"created_by", user}}, header_text, config)
    }

    if number_of_load_birds > 0 {
        fmt.Print(format_header("Load Birds", header_text))
        start := time.Now()
        lb := []interface{}{}
        sizes := [...]string{"small", "medium", "large"}
        colors := [...]string{"dull", "light", "bright", "dark", "guess"}
        eats_insects := [...]bool{true, false}
        habitats := [...]string{"north", "south", "everywhere", "east", "west", "midwest", "tropics", "africa", "woodlands", "plains"}
        ranks := [...]int{0,1,2,3,4,5,6,7,8,9}
        for i := 0; i < int(number_of_load_birds); i++ {
            bird := Bird{
                Name: "Testee " + strconv.Itoa(i),
                Size: sizes[rand.Intn(len(sizes))],
                Color: colors[rand.Intn(len(colors))],
                Habitat: habitats[rand.Intn(len(habitats))],
                Eats_insects: eats_insects[rand.Intn(len(eats_insects))],
                Rank: uint8(ranks[rand.Intn(len(ranks))]),
                Comment: "",
                Source: "lb",
                Created_by: user,
            }
            lb = append(lb, bird)
            // commit the batch and reset array.
            if math.Mod(float64(i), insert_queue_amount) == 0 {
                _, err := birds.InsertMany(ctx, lb)
                if err != nil {
                    log.Fatal(err)
                }
                lb = nil
            }
        }
        if len(lb) > 0 {
            _, err := birds.InsertMany(ctx, lb)
            if err != nil {
                log.Fatal(err)
            }
        }
        fmt.Println("- Bird Count:", count_birds())
        fmt.Println("- Last Bird: ", last_bird(config).Name)
        fmt.Println("- Load Time: ", time.Since(start).Seconds())

        // cleanup load birds.
        if config.Get("data.cleanup_load_birds").(bool) {
            fmt.Println("- cleaning up load_birds data...")
            result, err := birds.DeleteMany(ctx, bson.M{"source": "lb"})
            if err != nil {
                log.Fatal(err)
            }
            fmt.Printf("  - deleted: %v\n", result.DeletedCount)
        }
    }

    fmt.Print(format_header("RUN SUMMARY", header_text))
    fmt.Println("- Bird Count:        ", count_birds())
    fmt.Println("- Load Birds:        ", number_of_load_birds)
    fmt.Println("- Cleanup Load Birds:", config.Get("data.cleanup_load_birds").(bool))
    fmt.Println("- iQA:", insert_queue_amount)
}
