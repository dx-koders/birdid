package main

import (
   "context"
   "fmt"
   "log"
   "time"

   "go.mongodb.org/mongo-driver/bson"
   "go.mongodb.org/mongo-driver/mongo"
   "go.mongodb.org/mongo-driver/mongo/options"
   //~ "go.mongodb.org/mongo-driver/mongo/readpref"
)

type Post struct {

    Title string `json:"title,omitempty"`
    Body string `json:"body,omitempty"`

}

func main() {
    // note: you need to manually create the db db and the mongo/mongo user first!
    client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://mongo:mongo@localhost:27017/db"))
    if err != nil {
        log.Fatal(err)
    }

    ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
    err = client.Connect(ctx)
    if err != nil {
            log.Fatal(err)
    }

    // Insert Post
    post := Post{"Hello, World!", "This is where we say hello...and do something interesting!"}

    collection := client.Database("db").Collection("posts")

    insertResult, err := collection.InsertOne(context.TODO(), post)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println("Inserted post with ID:", insertResult.InsertedID)

    // Get Post
    collection = client.Database("db").Collection("posts")

    filter := bson.D{{}}

    var find_post Post

    err = collection.FindOne(context.TODO(), filter).Decode(&find_post)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Printf("\n\nFound post with title: %s and body: %s\n\n\n", find_post.Title, find_post.Body)

    //~ count, err := collection.CountDocuments(ctx, nil, nil)
    count, err := client.Database("db").Collection("posts").CountDocuments(ctx, bson.M{}, nil)
    if err != nil {
        log.Fatal("Error getting count: ", err)
    }

    fmt.Printf("\nNumber of posts: %d\n", count)


    defer client.Disconnect(ctx)
}
