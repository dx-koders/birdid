# README

Simply start the db and perform the 'Setup mongodb' steps below. If you want to load the birds.csv data see the following 'Import birds.csv' section.

### Setup mongodb ###

- switch to db database (this will create it): `use db`
- create user:

```
db.createUser(
  {
    user: "mongo",
    pwd: "mongo",
    roles: [ { role: "readWrite", db: "db" } ]
  }
)
```


### Import birds.csv ###

- use mongoimport from within container: `mongoimport --db=db --collection birds --file /opt/data/birds.csv --type csv --headerline`
- change eats_insects from string to bool:

```
db.birds.update(
    {},
    [{ $set: {eats_insects: { $toBool: "$eats_insects" } } }],
    { multi: true }
)
db.birds.update(
    { $or: [ {"name": "vulture"}, {"name": "hawk"}]},
    [{ $set: {eats_insects: false } }],
    { multi: true }
)
```
