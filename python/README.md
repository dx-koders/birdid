# README


### Setup ###

- create a virtual environment in your python root: `python3 -m virtualenv .venv`
- activate the virtualenv: `source .env-setup`
- install packages: `pip install -r requriements.txt`


### Links ###

- [PyMongo](https://pymongo.readthedocs.io/en/stable/): mongodb driver for python
- [Motor](https://docs.mongodb.com/drivers/motor/): we should look into this for db performance improvement
- [pyfiglet](https://pypi.org/project/pyfiglet/)
    - [pyfiglet github](https://github.com/pwaller/pyfiglet)
- [python-dotenv](https://pypi.org/project/python-dotenv/)
    - [python-dotenv gethub](https://github.com/theskumar/python-dotenv)
