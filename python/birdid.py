#!/usr/bin/env python3
# run: python3 birdid.py

import csv
import os
import sys
import toml

from dotenv import load_dotenv
from pathlib import Path
from pyfiglet import Figlet
from pymongo import MongoClient, ASCENDING, DESCENDING
from timeit import default_timer as timer

conf = toml.load("../data/config.toml")
client = MongoClient(conf["data"]["mongo"]["url"])

load_dotenv(dotenv_path=Path('./..') / '.env')
user = os.getenv('BIRD_USER')
if not user or len(user) < 3:
    sys.exit("User is not valid!\n - user must be specified and must be at least 3 characters long.")

def format_bird(bird, fmt):
    if fmt == "short":
        return f"- {bird['name'].title()}"
    elif fmt == "compact":
        return f"- {bird['name'].title()}: {bird['size']}, {bird['color']}, {bird['habitat']}, {bird['eats_insects']}, {bird['rank']}, {bird['comment']}, {bird['source']}, {bird['created_by']}"
    elif fmt == "full":
        return f"""
{bird['name'].title()}
    - size:      {bird['size']}
    - color:     {bird['color']}
    - habitat:   {bird['habitat']}
    - eats bugs: {bird['eats_insects']}
    - rank:      {bird['rank']}
    - comment:   {bird['comment']}
    - source:    {bird['source']}
    - creator:   {bird['created_by']}"""
    else:
        print(f"Error in formatting bird, unknown format: '{fmt}'")
        return ""

def show_header(title):
    print(f"\n{conf['display']['header_text']} {title} {conf['display']['header_text']}")

def show_birds(title, flt: {}, lmt=100):
    # @todo: sort can be abstracted
    bird_sort = [('$natural', ASCENDING)]
    if conf['data']['sort']['birds'] == 'alphabetical':
        bird_sort = [('name', ASCENDING)]
    show_header(title)
    for bird in client.db.birds.find(flt).sort(bird_sort).limit(lmt):
        print(format_bird(bird, "compact"))

def first_bird():
    # @todo: sort can be abstracted
    bird_sort = None
    if conf['data']['sort']['birds'] == 'alphabetical' :
        bird_sort = [('name', ASCENDING)]
    return client.db.birds.find_one({}, sort=bird_sort)['name']

def last_bird():
    # @todo: sort can be abstracted
    bird_sort = [('$natural', DESCENDING)]
    if conf['data']['sort']['birds'] == 'alphabetical' :
        bird_sort = [('name', DESCENDING)]
    return client.db.birds.find().limit(1).sort(bird_sort)[0]['name']

def get_bird(bird_name):
    return client.db.birds.find_one({"name": bird_name})

def add_bird(response):
    if response.lower() == "y":
        bird_name = input("  What is the bird name? ").lower()
        bird_size = input("  What is the bird size? ").lower()
        bird_color = input("  What is the bird color? ").lower()
        bird_habitat = input("  What is the bird habitat? ").lower()
        bird_ei = input("  Does this bird eat insects? [y/n] ").lower()
        bird_rank = input("  What is the bird's rank (9 being best)? [0-9] ")
        bird_comment = input("  Any comments about the bird? ")

        bird_eats_insects = True if bird_ei == "y" else False

        confirm = input(f"""
Confirm Add Bird:
  - name: {bird_name}
  - size: {bird_size}
  - color: {bird_color}
  - habitat: {bird_habitat}
  - eat insects: {bird_eats_insects}
  - rank: {bird_rank}
  - comment: {bird_comment}
Are you sure you want to add the bird? [y/n]: """)
        if confirm.lower() == "y":
            client.db.birds.update_one(
                {"name": bird_name},
                {'$set':
                    {"name": bird_name, "size": bird_size, "color": bird_color, "habitat": bird_habitat, "eats_insects": bird_eats_insects, "rank": int(bird_rank), "comment": bird_comment, "source": "birdid", "created_by": user}
                },
                upsert=True,
            )
            print(f"Bird '{bird_name.title()}' has been added!")
        else:
            print("Add/update bird canceled.")
        add_another_response = input("Do you want to add/update another bird? [y/n]: ")
        add_bird(add_another_response)

def delete_bird(response):
    if response.lower() == "y":
        bird_name = input("  Name of bird to delete: ").lower()
        bird = get_bird(bird_name)
        if not bird:
            print(f"Cannot Delete Bird!\n  Bird not found.")
        elif bird['created_by'] != user:
            print(f"Cannot Delete Bird!\n  You did not add '{bird['name']}', it was created by: {bird['created_by']}")
        else:
            print(f"  - deleting bird: {bird['name']}")
            result = client.db.birds.delete_one({"name": bird["name"]})
            print(f"    - deleted {result.deleted_count} bird")
        delete_another = input("Do you want to delete another bird? [y/n]: ").lower()
        delete_bird(delete_another)

def lookup_bird(response):
    if response.lower() == "y":
        bird_name = input("  Name of bird to find: ").lower()
        bird = get_bird(bird_name)
        if not bird:
            print(f"\nBird not found!")
        else:
            print(format_bird(bird, "full"))
        lookup_another = input("Do you want to find another bird? [y/n]: ").lower()
        lookup_bird(lookup_another)

print(conf["display"]["logo"])
print(f"{conf['about']['name']} - v{conf['about']['python']['version']} (modified: {conf['about']['python']['modified']}) - © {conf['about']['python']['authors']}")
print(conf['display']['separator'])
f = Figlet(font=conf['display']['user_logo_font'])
print(f.renderText(f'Hi {user}!'))

number_of_load_birds = 0
if conf['options']['show_load_birds']:
    number_of_load_birds = int(input("Enter number of load birds: "))
show_big_birds = "n"
if conf['options']['show_big_birds']:
    show_big_birds = input("Do you want to show big birds? [y/n]: ")
show_birds_who_eat_insects = input("Do you want to show birds who eat insects? [y/n]: ")
add_bird_response = input("Do you want to add/update a bird? [y/n]: ")
add_bird(add_bird_response)
delete_bird_response = input("Do you want to delete a bird? [y/n]: ")
delete_bird(delete_bird_response)
lookup_bird_response = input("Do you want to look up a bird? [y/n]: ")
lookup_bird(lookup_bird_response)

print(conf['display']['separator'])

print(f"- Total Birds: {client.db.birds.count_documents({})}")
print(f"- First Bird:  {first_bird()}")
print(f"- Last Bird:   {last_bird()}")

if conf['options']['show_all_birds']:
    show_birds("All Birds", {})

if conf['options']['show_small_birds']:
    show_birds("Small Birds", {"size":"small"})

if conf['options']['show_bright_birds']:
    show_birds("Bright Birds", {"color":"bright"})

if show_big_birds == "y":
    show_birds("Big Birds", {"size":"large"})

if show_birds_who_eat_insects.lower() == "y":
    show_birds("Buggers", {"eats_insects": True})
else:
    show_birds("No Buggers", {"eats_insects":False})

if number_of_load_birds > 0:
    show_header(f"Load Birds: {number_of_load_birds:,}")
    lb = []
    start = timer()
    for i in range(1,number_of_load_birds + 1):
        lb.append({ "name": f"Testee {i}", "size": "medium", "color": "light", "habitat": "everywhere", "eats_insets": True, "rank": 5, "comment": "", "source": "lb", "created_by": user })
        if (i % conf['data']['insert_queue_amount']) == 0:
            client.db.birds.insert_many(lb, ordered=False)
            lb = []

    if len(lb) > 0:
        client.db.birds.insert_many(lb, ordered=False)

    print(f"- Birds Count: {client.db.birds.count_documents({})}")
    print(f"- Last Bird:   {last_bird()}")
    finish = timer()
    print(f"- Load Time:   {finish - start}")

    if conf['data']['cleanup_load_birds']:
        print(f"- cleaning up load_birds data...")
        dc = client.db.birds.delete_many({"source": "lb"})
        print(f"  - deleted: {dc.deleted_count:,}")

show_header("RUN SUMMARY")
print(f"- Bird Count:         {client.db.birds.count_documents({})}")
print(f"- Load Birds:         {number_of_load_birds:,}")
print(f"- Cleanup Load Birds: {conf['data']['cleanup_load_birds']}")
print(f"- iQA: {conf['data']['insert_queue_amount']}")
