CREATE TABLE public.birds (
    id SERIAL,
    "name" varchar(100) NOT NULL,
    "size" varchar(25) NOT NULL,
    color varchar(25) NOT NULL,
    habitat varchar(30) NOT NULL,
    eats_insects boolean NOT null default true,
    "rank" smallint NOT null default 5,
    "comment" text NULL,
    CONSTRAINT birds_pk PRIMARY KEY (id)
);
