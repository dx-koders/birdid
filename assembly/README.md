# README


### Setup ###

- install nasm compiler: `apt install nasm`

### Compile ###

You may want compile/run the hello program first:
```
nasm -f elf64 hello.asm # assemble the program
ld -s -o hello hello.o # link the object file nasm produced into an executable file
./hello # hello is an executable file
```
To compile and run birdid:
```
nasm -f elf64 birdid.asm # assemble the program
ld -s -o birdid birdid.o # link the object file nasm produced into an executable file
./birdid # hello is an executable file
```
