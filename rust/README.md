# README

You will need to install rust before you can compile the app.

### Install ###

- install rust: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
    - [learn more about installing rust](https://www.rust-lang.org/learn/get-started)

### Run App ###

- compile and run with: `cargo run`
    - first run may take a little longer as it needs to install dependencies

### Useful Links ###

- toml
    + [toml crate](https://docs.rs/toml/0.5.8/toml/)
- db
    + [diesel](http://diesel.rs/): db orm
    + [mongodb driver](https://docs.rs/mongodb/1.1.1/mongodb/)
- ascii-art
    + [rs-figlet](https://github.com/yuanbohan/rs-figlet)
    + [figlet.org](http://www.figlet.org/fontdb.cgi) - download more figlets
