// rust does not need a declaration, it is compiled.
// run: cargo run

extern crate csv;
extern crate dotenv;
extern crate dotenv_codegen;
extern crate toml;
//~ #[macro_use]
extern crate serde_derive;

use std::time::Instant;
use std::error::Error;
use std::fs;
use text_io::read;
use std::io;
use std::io::Write;
use inflector::Inflector;
use std::mem;
use figlet_rs::FIGfont;

use dotenv::dotenv;
use dotenv_codegen::dotenv;

use serde::{Deserialize, Serialize};
use mongodb::{
    bson::{self, doc, Bson},
    sync::Client,   // sync client
    sync::Collection,
    // Client,   // async client
    // options::ClientOptions
    options:: {
        //~ Acknowledgment,
        FindOptions,
        //~ InsertManyOptions,
        //~ WriteConcern,
    }
};

#[derive(Serialize, Deserialize, Debug)]
struct Bird {
    //~ #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    name: String,
    size: String,
    color: String,
    habitat: String,
    rank: i32,
    eats_insects: bool,
    comment: String,
    source: String,
    created_by: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Lang {
    created: String,
    modified: String,
    version: String,
    authors: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct About {
    name: String,
    go: Lang,
    python: Lang,
    ruby: Lang,
    rust: Lang,
}

#[derive(Serialize, Deserialize, Debug)]
struct DataPostgres {
    host: String,
    port: u32,
    user: String,
    pass: String,
    name: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct DataMongo {
    url: String
}

#[derive(Serialize, Deserialize, Debug)]
struct DataSort {
    birds: String
}

#[derive(Serialize, Deserialize, Debug)]
struct Data {
    insert_queue_amount: u32,
    cleanup_load_birds: bool,
    mongo: DataMongo,
    sort: DataSort,
}

#[derive(Serialize, Deserialize, Debug)]
struct Display {
    separator: String,
    header_text: String,
    logo: String,
    bird_line_format: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ConfOptions {
    show_all_birds: bool,
    show_small_birds: bool,
    show_bright_birds: bool,
    show_dull_birds: bool,
    show_load_birds: bool,
    show_big_birds: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct Conf {
    about: About,
    data: Data,
    display: Display,
    options: ConfOptions,
}

fn show_header(title: &str, header_text: &str) {
    println!("\n{} {} {}", header_text, title, header_text);
}

fn show_birds(collection: &Collection, title: &str, cfg: &Conf, filter: mongodb::bson::Document) {
    let mut sort = doc! {};
    if cfg.data.sort.birds == "alphabetical" {
        sort = doc! { "name": 1 };
    }
    let options = FindOptions::builder().sort(sort).limit(100).build();
    show_header(title, &cfg.display.header_text);
    let cursor = collection.find(filter, options).unwrap();
    for result in cursor {
        //~ let doc = result.expect("Received network error during cursor operations.");
        //~ if let Some(&Bson::String(ref value)) = doc.get("name") {
            //~ println!("- {}", value);
        //~ }
        let lb = result.unwrap();
        let bird: Bird = bson::from_bson(bson::Bson::Document(lb)).unwrap();
        println!("{}", format_bird(&bird, &cfg.display.bird_line_format));
        //println!("- bird: {:?}", bird);
    }
}

fn add_bird(collection: &Collection, response: &str, user: &str) {
    if response.to_ascii_lowercase() == "y" {
        let bird_name = get_user_input("  What is the bird name? ").to_ascii_lowercase();
        let bird_size = get_user_input("  What is the bird size? ").to_ascii_lowercase();
        let bird_color = get_user_input("  What is the bird color? ").to_ascii_lowercase();
        let bird_habitat = get_user_input("  What is the bird habitat? ").to_ascii_lowercase();
        let bird_ei = get_user_input("  Does this bird eat insects? [y/n] ").to_ascii_lowercase();
        let bird_rnk = get_user_input("  What is the bird's rank (9 being best)? [0-9] ");
        let bird_comment = get_user_input("  Any comments about the bird? ");

        let bird_eats_insects = if bird_ei == "y" { true } else { false };
        let bird_rank: i32 = bird_rnk.parse().unwrap_or(0);
        let confirm_msg = format!("Confirm Add/Update Bird:
  - name:         {}
  - size:         {}
  - color:        {}
  - habitat:      {}
  - eats insects: {}
  - rank:         {}
  - comment:      {}
Are you sure you want to add/update the bird? [y/n] ", bird_name, bird_size, bird_color, bird_habitat, bird_eats_insects, bird_rank, bird_comment);
        let confirm = get_user_input(&confirm_msg).to_ascii_lowercase();
        if confirm == "y" {
            println!("  - add/update bird: {}", &bird_name);
            let query = doc! {"name": &bird_name};
            let bird = doc! { "name": &bird_name, "size": bird_size, "color": bird_color, "habitat": bird_habitat, "eats_insects": bird_eats_insects, "rank": bird_rank, "comment": bird_comment, "source": "birdid", "created_by": user };
            let options = mongodb::options::UpdateOptions::builder().upsert(true).build();
            let result = collection.update_one(query, bird, options).unwrap();
            println!("  matched/modified: {}/{}", result.matched_count, result.modified_count);
            let add_another = get_user_input("Do you want to add/update another bird? [y/n] ").to_ascii_lowercase();
            add_bird(&collection, &add_another, &user);
        } else {
            println!("Add/update bird canceled.");
        }
    }
}

fn find_bird(collection: &Collection, bird_name: &str) -> Option<Bird> {
    let response = collection.find_one(Some(doc! { "name":  &bird_name }), None).unwrap();
    if response.is_some() {
        let lb = response.unwrap();
        let bird: Bird = bson::from_bson(bson::Bson::Document(lb)).unwrap();
        return Some(bird);
    }
    // return none.
    None
}

fn delete_bird(collection: &Collection, response: &str, user: &str) {
    if response.to_ascii_lowercase() == "y" {
        let bird_name = get_user_input("  Name of bird to delete? ").to_ascii_lowercase();
        let bird_w = find_bird(&collection, &bird_name);
        if bird_w.is_some() {
            let bird = bird_w.unwrap();
            if  bird.created_by == user {
                println!("  - deleting bird {}: ", &bird_name);
                collection.delete_one(doc !{"name": &bird_name}, None).expect("Error updating bird!");
                println!("    - deleted bird");
            } else {
                println!("Cannot Delete Bird!\n  You did not add '{}', it was created by: {}\n", bird.name, bird.created_by)
            }
        } else {
            println!("\nCannot Find Bird: '{}'", bird_name);
        }
        let delete_another = get_user_input("Do you want to delete another bird? [y/n] ").to_ascii_lowercase();
        delete_bird(&collection, &delete_another, &user);
    }
}

fn format_bird(bird: &Bird, bird_line_format: &str) -> String {
    if bird_line_format == "short" {
        return format!("- {}", bird.name.to_title_case());
    } else if bird_line_format == "compact" {
        return format!("- {}: {}, {}, {}, {}, {}, {}, {}, {}", bird.name.to_title_case(), bird.size, bird.color, bird.habitat, bird.eats_insects, bird.rank, bird.comment, bird.source, bird.created_by);
    } else if bird_line_format == "full" {
        return format!("
{}
    - size:      {}
    - color:     {}
    - habitat:   {}
    - eats bugs: {}
    - rank:      {}
    - comment:   {}
    - source:    {}
    - creator:   {}",
  bird.name.to_title_case(), bird.size, bird.color, bird.habitat, bird.eats_insects, bird.rank, bird.comment, bird.source, bird.created_by);
    } else {
        // unknown format!
        return "".to_string();
    }
}

fn lookup_bird(collection: &Collection, response: &str) {
    if response.to_ascii_lowercase() == "y" {
        let bird_name = get_user_input("  Name of bird to find? ").to_ascii_lowercase();
        let bird = find_bird(&collection, &bird_name);
        if bird.is_some() {
            println!("{}", format_bird(&bird.unwrap(), "full"));
        } else {
            println!("\nCannot Find Bird: '{}'", bird_name);
        }
        let lookup_another = get_user_input("Do you want to find another bird? [y/n] ").to_ascii_lowercase();
        lookup_bird(&collection, &lookup_another);
    }
}

fn get_first_bird(collection: &Collection, cfg: &Conf) -> String {
    let mut sort = doc! {};
    if cfg.data.sort.birds == "alphabetical" {
        sort = doc! { "name": 1 };
    }
    let options = FindOptions::builder().sort(sort).limit(1).build();
    let cursor = collection.find(doc! {}, options).unwrap();
    for result in cursor {
        let doc = result.expect("Received network error during cursor operations.");
        if let Some(&Bson::String(ref value)) = doc.get("name") {
            return value.to_string();
        }
    }
    // if nothing found.
    return "".to_string();
}

fn get_last_bird(collection: &Collection, cfg: &Conf) -> String {
    let mut sort = doc! { "_id": -1 };
    if cfg.data.sort.birds == "alphabetical" {
        sort = doc! { "name": -1 };
    }
    let options = FindOptions::builder().sort(sort).limit(1).build();
    let cursor = collection.find(doc! {}, options).unwrap();
    for result in cursor {
        let doc = result.expect("Received network error during cursor operations.");
        if let Some(&Bson::String(ref value)) = doc.get("name") {
            return value.to_string();
        }
    }
    // if nothing found.
    return "".to_string();
}

fn get_user_input(prompt: &str) -> String {
    print!("{}", prompt);
    io::stdout().flush().unwrap();
    return read!("{}\n");
}

fn main() -> Result<(), Box<dyn Error + 'static>> {
    dotenv().ok();
    let user = dotenv!("BIRD_USER");
    if user.len() < 3 {
        panic!("User is not valid!\n - user must be specified and must be at least 3 characters long.")
    }

    let conf_file = fs::read_to_string("../../data/config.toml")
        .expect("Something went wrong reading the file");
    // println!("With text:\n{}", conf);
    //~ println!("- conf is of type: {}", conf_file.what_is_my_type());
    let conf: Conf = toml::from_str(&conf_file).unwrap();
    //~ let conf: toml::Value = toml::from_str(&conf)?;
    //~ println!("{:?}", conf);
    let client = Client::with_uri_str(&conf.data.mongo.url)?;
    let db = client.database("db");
    let collection = db.collection("birds");
    let bird_count = collection.count_documents(doc! {}, None)?;

    println!("{}", conf.display.logo);
    println!("   {} - v{} ({}) - © {}", conf.about.name, conf.about.rust.version, conf.about.rust.modified, conf.about.rust.authors);
    print!("{}", conf.display.separator);

    //~ let standard_font = FIGfont::standand().unwrap();
    //~ let figure = standard_font.convert("Hi drad!");
    let smslant_font = FIGfont::from_file("resources/smslant.flf").unwrap();
    let figure = smslant_font.convert(&format!("Hi {}!", user));
    println!("{}", figure.unwrap());

    let mut number_of_load_birds = 0;
    if conf.options.show_load_birds {
        number_of_load_birds = get_user_input("Enter number of load birds: ").parse::<u32>().unwrap();
    }

    let show_big_birds = get_user_input("Do you want to show big birds? [y/n]: ");
    let show_birds_who_eat_insects = get_user_input("Do you want to show birds who eat insects (y) or birds who do not eat insects (n)? [y/n]: ");

    let add_bird_response = get_user_input("Do you want to add/update a bird? [y/n]: ");
    add_bird(&collection, &add_bird_response, &user);
    let delete_bird_response = get_user_input("Do you want to delete a bird? [y/n]: ");
    delete_bird(&collection, &delete_bird_response, &user);
    let get_bird_response = get_user_input("Do you want to look up a bird? [y/n]: ");
    lookup_bird(&collection, &get_bird_response);

    println!("{}", conf.display.separator);

    println!("- Total Birds: {}", bird_count);
    println!("- First Bird:  {}", get_first_bird(&collection, &conf).to_title_case());
    println!("- Last Bird:   {}", get_last_bird(&collection, &conf).to_title_case());

    if conf.options.show_all_birds {
        show_birds(&collection, "All Birds", &conf, doc! {});
    }

    if conf.options.show_small_birds {
        show_birds(&collection, "Small Birds", &conf, doc! {"size": "small"});
    }

    if conf.options.show_bright_birds {
        show_birds(&collection, "Bright Birds", &conf, doc! {"color": "bright"});
    }

    if show_big_birds == "y" {
        show_birds(&collection, "Big Birds", &conf, doc! {"size":"large"});
    }

    if show_birds_who_eat_insects == "y" {
        show_birds(&collection, "Buggers", &conf, doc!{"eats_insects":true});
    } else {
        show_birds(&collection, "No Buggers", &conf, doc!{"eats_insects":false});
    }

    if number_of_load_birds > 0 {
        let title: String = format!("Load Birds: {}", number_of_load_birds);
        show_header(&title, &conf.display.header_text);
        let mut lb = vec![];
        let start = Instant::now();
        for i in 1..number_of_load_birds + 1 {
            let bird = doc! { "name": &("Testee X".to_owned() + &i.to_string()), "size": "medium", "color": "light", "habitat": "everywhere", "eats_insets": true, "rank": 5, "comment": "load_birds" };
            // direct inserts
            //~ collection.insert_one(bird, None)?;
            // bulk inserts
            lb.push(bird);
            //~ println!("- pushed load_bird: {} - {}", i, lb.len());
            // check if we 'insert'
            if (i % conf.data.insert_queue_amount) == 0 {
                // tmp vec to copy for insert.
                let mut lb_tmp = vec![];
                mem::swap(&mut lb, &mut lb_tmp); // lb <-> lb_tmp
                //~ println!("- performing insert_many with size: {}", lb_tmp.len());
                //~ collection.insert_many(lb_tmp, Some(
                    //~ InsertManyOptions::builder()
                        //~ .ordered(false)
                        //~ .bypass_document_validation(true)
                        //~ .write_concern(WriteConcern::builder().w(Acknowledgment::Majority).build())
                        //~ .build()
                //~ )).expect("Something went wrong on bulk insert");
                collection.insert_many(lb_tmp, None)?;
            }
        }
        // insert any remaining.
        if lb.len() > 0 {
            //~ println!("- insert_many of remaining...");
            //~ collection.insert_many(lb, Some(
                //~ InsertManyOptions::builder()
                    //~ // .write_concern(WriteConcern::builder().w(Acknowledgment::Majority).build())
                    //~ .ordered(false)
                    //~ .bypass_document_validation(true)
                    //~ .build()
            //~ )).expect("Something went wrong on bulk insert");
            collection.insert_many(lb, None)?;
            //~ println!("- after remaining insert many...");
        }

        let lb_last_bird: String = get_last_bird(&collection, &conf);
        let lb_bird_count = collection.count_documents(doc! {}, None)?;
        println!("- Birds Count: {}", lb_bird_count);
        println!("- Last Bird:   {}", lb_last_bird);
        println!("- Load Time:   {:?}", start.elapsed());
        if conf.data.cleanup_load_birds {
            println!("- cleaning up load birds data...");
            let dc = collection.delete_many(doc! {"comment": "load_birds"}, None)?;
            println!("  - deleted: {}", dc.deleted_count);
        }
    }

    show_header("RUN SUMMARY", &conf.display.header_text);
    let final_bird_count = collection.count_documents(doc! {}, None)?;
    println!("- Bird Count:         {}", final_bird_count);
    println!("- Load Birds:         {}", number_of_load_birds);
    println!("- Cleanup Load Birds: {}", conf.data.cleanup_load_birds);
    println!("- iQA:                {}", conf.data.insert_queue_amount);

    Ok(())
}
