# Class 3 Supplemental

Extra work for class 3.

### Tips ###

- useful git aliases:

```
# git
alias gits='git status'
alias gitv='git rev-parse --short HEAD'
alias gita='git add'
alias gitaa='git add --all'
alias gitc='git commit'
alias gitd='git difftool'
alias gitl='git log -1 --pretty=%B | xclip -in -selection clipboard && echo "last log message copied to clipboard"'
alias gitmf='git merge --no-ff'
```


### Homework ###

#### Add 3 New Birds ####

Add 3 new birds. We want the new birds available to everyone so we will do it off of the master branch, commit and push after you have added them.


#### Add to Your Code ####

Add code (your language) the following:

- ask user to display birds who eat insects or birds who do not eat insets and show appropriate set of birds
- make the title show which group; for example:

```
########### Birds Who Eat Insects ###########
- chickadee
- wren
- nuthatch
```

  - or -

```
########### Birds Who Do Not Eat Insects ###########
- hawk
- vulture
```

- add an about structure and display it directly after the logo; it should have the following variables:
    - created
    - modified
    - name
    - authors
    - version
    it should display like the following:

```
____________________________________________
    ____
    /   )    ,             /       ,       /
---/__ /--------)__----__-/------------__-/-
  /    )   /   /   ) /   /  ===  /   /   /
_/____/___/___/_____(___/_______/___(___/___


bird-id - v.1.0.0 (modified: 2020-01-01) - © David Rader
```
