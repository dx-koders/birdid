# Goal

The goal of this project is to provide a tangible application which can be used to teach the basic concepts of computer programming.

### Basic Concepts ###

- basic concepts of any language (variables, structures, syntax)
- implement an application in several languages to show differences
- show basic concepts in each language
    - variables
    - conditions
    - loops
    - structures (basics of how to model)
- add common concepts in each language
    - abstractions
    - config files
    - data files
    - inputs

Need a concept simple enough that we can implement it in several languages, frameworks and interfaces.

Also learning how to 'design' an application.

- find a topic: bird id
- assess the needs
    - data structures to store birds
    - a way to ask questions and have it answer them for you: (what color?, size?, habitat? --> must be a bluejay)
    - how will we interact?
        - we will answer a set of questions, it will show us the bird/birds which match
        - need to add, edit and remove birds

I like the idea of using our base app as the api to which we may make a cli and will likely make a quasar UI at some point.
