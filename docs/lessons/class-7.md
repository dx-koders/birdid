# Programming Class 7

### Notice ###

- see the `homework.md` file for help on when homework is due, how to submit it and more.


### Goal ###

Use database to store our data and use more functions.


### Agenda ###

- What Changed
    + added bird sort logic (can be changed via config item)
    + added update bird (and upsert for dedup on name) logic
    + added user logic with user_logo
        * all birds added or edited will have your username
    + added `show your birds`
    + added delete bird logic


### Discussion ###

- a better look at a function
    + reusable group of code
    + keywork to identify it as a function (def, fn, func)
    + has input and output parameters
- review sort logic
- review update / upsert logic
- review user logic (.env and asciiart)


### Homework ####

- review [homework](homework.md) for how to do homework, points, and how to turn in homework
- show only your birds (see `birdid.go` for example)
    + label it 'My Birds'
    + put it after 'No Buggers'
    + ask user if they would like to show their own birds, if not do not display them


### Bonus ###

- change all birds output (1 Exta Point)
    - add a new bird_line_format (we have full, compact, and short - make a new one) and use it!


### Next Week ###

- move items from config to .env
- add questions
    + add to lookup bird the ability to lookup on other things and use it to show a bird or set of birds
        - color, size, habitat
