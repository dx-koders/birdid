# Intro to programming; basic components of any language.

- Variables: a way to store information
- Control Structures: a decision the computer makes
- Data Structures: a way of storing data
- Syntax: the rules of a particular programming language

### Variables ###

A storage location and an associated symbolic name which contains some known or unknown quantity or information, a value.

#### Example ####

Say we are storing information about a person, what variables would we use?

- name
- age
- date of birth
- hair color
- gender
- email address

### Control Structures ###

A control structure is a block of programming that analyzes variables and chooses a direction in which to go based on given parameters. The term flow control details the direction the program takes (which way program control “flows”). Hence it is the basic decision-making process in computing; flow control determines how a computer will respond when given certain conditions and parameters.

#### Example ####

Say you are reading a book about birds and the index says for Cardinals go to page 13. That decision process is the Control Flow or Control Structure of the program.

``` python
if age > 12 and age < 20:
  print("you are a teenager")
else:
  print("you are not a teenager")
```

### Data Structures ###

A data structure is a particular way of storing and organizing data in a computer so that it can be used efficiently.

#### Example ####

Lets store the names of 3 people as an example.

``` python
person1 = "Natalie Rader"
person2 = "Addalie Rader"
person3 = "Kailie Rader"
```

Another way using an array:

``` python
person = []
person.append("Natalie Rader")
person.append("Addalie Rader")
person.append("Kailie Rader")
```

### Syntax ###

The syntax of a programming language is the set of rules that define the combinations of symbols that are considered to be correctly structured programs in that language.


``` python
age = 10
if age < 20:
  print("age is less than 20")
else:
  print("age is not less than 20")
```

``` java
Int     age = 20;
if (age < 20) {
  System.out.println("age is less than 20");
} else {
  System.out.println("age is not less than 20");
}
```

### Links ###

- [CodersCampus](https://howtoprogramwithjava.com/the-5-basic-concepts-of-any-programming-language-concept-5/) - general structure/info from this site
