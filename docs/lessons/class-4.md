# Programming Class 4

### Goal ###

Continue with abstraction but focus on control structure abstraction this time. We will also talk about databases for better data storage.


### Agenda ###

In the class we will add to our bird id application:

- review of data abstraction
    - what did we abstract?
    - why did we abstract?
- introduction to functions/methods
    - start with class-3 code and have them change headers to something else
- review our code to see if there is anything that may benefit from control structure abstraction
    - change headers from ####### to use ------
    - looks like section header `fmt.Println("\n\n########### All Birds ###########");` repeats
        - looking further, the entire 'show filtered bird list' is repeated several times


### Code Changes ###

- added function make_header
- created separator
- added function eats_insects_label
- added function show_filtered_birds
- changed logo


### Databases ###

What is a database?

Why do we need a database?

What are some common databases?

Next week we will change our application to use a database!


### Process Change ###

All work needs to be done in a branch under your name. For instance:

- branch: `kai/class3`, `kai/class4`, `mein/class3`, `mein/class4`, `chunk/class3`, `chunk/class4`

If you want code applied to master let me know and I'll merge it.


### Bonus ###

- added Assembly - lets check it out!

### Homework ####

- apply data abstraction to your code!

### Next Week ###

- Intro to Databases
