# Homework

Due: Friday by 8pm.


### What Is Your Homework ###

See the homework section of the class notes.


### Where To Do Your Homework ###

Check out your homework in your branch:

    `get checkout $USER/<class>`
    - replace <class> with the class we are on (such as class6, class7, etc.). The class name will be provided to you in rocketchat when homework is available to be worked on.


### How To Submit Your Homework ###

Complete your homework and make sure you test it! Dont forget to update the config.toml file with the `about` info for your language!

When you are ready to turn in your homework:

- add it: `gitaa`
    + this adds all of your local changes to the list of files that will be committed
- check files to be committed: `gits`
    + make sure the files you expect to be committed are listed (usually in green - not in red)
- commit it: `gitc -m "describe your changes"`
    + this 'saves' all of your changes in your local git
- push it: `git push`
    + this uploads all of your changes to the remote git repo
- check everything: `gits`
    + you should see a message saying something like "Your branch is up to date with 'origin/<class>'", if not your homework is not completely submitted.

Once have have compeleted the above send me a message on rocketchat letting me know you are done so I can merge your changes into master.


### Points ###

You will receive points for doing homework using the following scale:

- 0 points: no homework or incomplete homework
- 1 point: complete homework
- 2 points: above and beyond (you did more than required in a big way)
- 3 points: amazing (you did something that is really amazing)

### Bonus Points ###

Bonus points will be awarded for (above and beyond) work. Each class usually has bonus points available (see the class notes). In addition to bonus points for the class are the following bonus points:

- 1 point: for doing all of your homework by yourself (no teacher help)
- 1 point: for the first person who mentions the "extra super secret bonus point" to the teacher
