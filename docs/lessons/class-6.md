# Programming Class 6

- source: class6
- homework: <user>/class6


### Goal ###

Use database to store our data and use more functions.


### Agenda ###

- What Changed
    + birds csv is now in a database
    + new config options
    + created add_bird function (you need to prompt user and call the function!)
    + if 0 load birds no birds loaded
    + added Run Summary


### Discussion ###

- what is a database, what types have we talked about?
- why use a database?
- what uses databases?


### Homework ####

- update config file with info (for your language)
- create new prompt to see if user wants to add a bird, if so call `add_bird` function!
- test

*note*: see `homework.md` file for help on how to submit homework once you are done.


### Bonus ###

- change all birds output (1 Exta Point)
    - show bird name in Proper Case. For example: Turkey, Vulture, Buzzard rather than turkey, vulture, buzzard
    - add color after name. For example Turkey (dark), Buzzard (dark), Goose (light)


### Next Week ###

- delete a bird
- sort birds
- ensure bird does not already exist
