# Programming Class 5

### Goal ###

Continue with abstraction but focus on control structure abstraction this time. We will also talk about databases for better data storage.

Introduction to databases!


### Agenda ###

In the class we will get an intro into databases and make some minor enhancements to our bird id application:

- databases
    + RDBMS (postgres, mysql, oracle, mssql, db2)
        - benefits: relationships, reliable
        - drawbacks: more work, structure changes are more difficult
    + Document Databases (mongo, couchdb, couchbase, elasticsearch)
        - benefits: faster, easy to change structure
        - drawbacks: backup/restore, relationships are difficult, lends to sloppy data models
- enhance our conf file
    + make proper sections
    + handle 'about' for each language
- add 'comment' to bird data (will use this later to show multi-comment per bird is issue)
- add databases via docker

### Code Changes ###

- conf
    + cleaned up general structure to be more logical
    + added language under about structure so we can have info per language
- more concise code with functions

### Discussion ###

- view data in dbs
    - load csv
    - app code for db
- is a database appropriate for our application?
- each person suggest a new item to add to our data model

### Bonus ###

- performance results for database

### Homework ####

- update config file with info for your language
- test
- cleanup your output
- cleanup your code

*note*: see `homework.md` file for help on how to submit homework once you are done.

### Next Week ###

- add a bird!
