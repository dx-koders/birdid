# Programming Class 3

### Purpose ###

Continue with the bird-id application in python, ruby, and rust.


### Agenda ###

In the class we will add to our bird id application:

- add program input (prompt for args)
- add data abstraction (to a file)
- add basic logic to ask a question and evaluate the answer
- config file

Changes:
- added rank and eats_insects to data for each bird
- added 2 new birds
- moved data to a csv file

### Bonus ###

Added the application in [Go](https://golang.org/doc/tutorial/getting-started).
