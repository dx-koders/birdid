# Programming Class 2

### Purpose ###

Take a concept and write it in 3 different languages:

- Python
- Ruby
- Rust

### Agenda ###

In the class we will:

- use variables: string, int, boolean, and array
- use data structures: basic objects
- use control structures: evaluations, loops
- show good and bad structure (can use load_birds to show this, with and without a function)
- deal with syntax for compiled vs dynamic languages
- show speed of language

### The Concept ###

We will create a bird id application. The application will have a list of birds and allow a user to ask questions to identify one of the birds in the list.

#### Id Questions ####

- What color is the bird (dark, light, bright, dull)?
- What size is the bird (small, medium, large)?
- Where does the bird live (northeast, midwest, southeast, northwest, southwest, north, south, east, west)?

### What We Need ###

- installed language
- data structure
    + bird
        * name: name of bird
        * size: small, medium, large
        * color: dark, light, bright, dull, plain
        * habitat: northeast, midwest, southeast, northwest, southwest, north, south, east, west
- list of birds

### Bonus ###

We will also load a large amount of 'Test Birds' into our application to see how volume impacts performance of the application and language being used.

### Homework ###

Everyone will check out code, create a branch, make a change and push the change.

- in a terminal: `cd ~/code`
- check out: `git clone git@gitlab.com:dx-koders/birdid.git`
- create your branch: `git checkout -b $USER-class2`
- make some changes to your program
- stage your changes: `git add --all`
- commit your changes: `git commit -m "my first change"`
- push your changes: `git push`
