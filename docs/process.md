

### Setup for Homework Process ###

- merge previous class work and any other work into current class first
- create everyone's branch, push it and reset local to point to correct remote.

```
branch_name="class7"
git checkout -b mein/${branch_name} origin/${branch_name}
git push origin HEAD
git branch mein/${branch_name} --set-upstream-to origin/mein/${branch_name}

git checkout -b chunk/${branch_name} origin/${branch_name}
git push origin HEAD
git branch chunk/${branch_name} --set-upstream-to origin/chunk/${branch_name}

git checkout -b kai/${branch_name} origin/${branch_name}
git push origin HEAD
git branch kai/${branch_name} --set-upstream-to origin/kai/${branch_name}
```

- post class name in #learn-coding RC channel:
    @all class7 homework is available, you can check it out with the following command: `get checkout $USER/class7`
